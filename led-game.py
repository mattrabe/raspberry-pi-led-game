import RPi.GPIO as GPIO
GPIO.setmode(GPIO.BCM)
import threading
import random
import time

# consts
buttonA = 23
buttonB = 24
buttons = [buttonA, buttonB]
redLed = 4
orangeLed = 17
greenLed = 27
blueLed = 22
leds = [redLed, orangeLed, greenLed, blueLed]
comboInterval = .5

# Setup inputs & outputs
for button in buttons:
    GPIO.setup(button, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
for led in leds:
    GPIO.setup(led, GPIO.OUT)

def watchInputs():
    GPIO.add_event_detect(buttonA, GPIO.BOTH, buttonChange)
    GPIO.add_event_detect(buttonB, GPIO.BOTH, buttonChange)

def stopWatchingInputs():
    GPIO.remove_event_detect(buttonA)
    GPIO.remove_event_detect(buttonB)

def buttonChange(channel):
    if (GPIO.input(channel)):
        global currentSelections

        if GPIO.input(buttonA):
            currentSelections.append(0)
        else:
            currentSelections.append(1)

        if (len(currentSelections) == 1):
            threading.Timer(comboInterval, processSelection).start()

def processSelection():
    global selections, currentSelections

    selection = None
    if (len(currentSelections) == 1):
        selection = currentSelections[0]
    elif (currentSelections[0] == 0 & currentSelections[0] == currentSelections[1]):
        selection = 2
    elif (currentSelections[0] == 1 & currentSelections[0] == currentSelections[1]):
        selection = 3

    if (selection != None):
        selections.append(selection)
        GPIO.output(leds[selection], 1)
        time.sleep(.2)
        GPIO.output(leds[selection], 0)
    
    currentSelections = []

def turnOnLed(index):
    GPIO.output(leds[index], 1)
def turnOnAllLeds():
    for i, led in enumerate(leds):
        turnOnLed(i)

def turnOffLed(index):
    GPIO.output(leds[index], 0)
def turnOffAllLeds():
    for i, led in enumerate(leds):
        turnOffLed(i)

def flashAllLeds(times):
    turnOnAllLeds()
    time.sleep(.2)
    turnOffAllLeds()
    if (times > 1):
        time.sleep(.2)
        flashAllLeds(times - 1)

def goodbye():
    print('Goodbye!')
    GPIO.cleanup()

alive = True
flashes = []
currentSelections = []
selections = []
try:
    flashAllLeds(5)
    time.sleep(2)

    while alive:
        print('')
        print('** Round '+str(len(selections) + 1)+' **')

        flashes.append(random.randint(0, 3))
        for flash in flashes:
            turnOffAllLeds()
            time.sleep(.1)
            GPIO.output(leds[flash], 1)
            time.sleep(1)
        turnOffAllLeds()
        flashAllLeds(1)

        selections = []
        watchInputs()
        print('Your turn...')
        turnLength = len(flashes) + 2
        print('Turn length: '+str(turnLength))
        time.sleep(turnLength)

        stopWatchingInputs()
        flashAllLeds(1)

        if (flashes != selections):
            print('You lose!')
            alive = False
            flashAllLeds(5)
            goodbye()

except KeyboardInterrupt:
    goodbye()

