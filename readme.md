# Raspberry Pi LED Game

This simple project is written in Python3, runs on a Raspberry Pi,  and uses switches on GPIO inputs to turn on & off LEDs on GPIO outputs.

## Circuit:

1. Schematic file is led-game-schematic.png (source is led-game-schematic.cddx and can be opened using https://www.circuit-diagram.org/)

## Dependencies:

1. Requires GRPi.GPIO (`pip3 install RPi.GPIO`)

## Running:

1. `python3 led-game.py`
